//ViewController.swift created on 12/02/2020

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var response: FiveDaysForecast?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        let APISource = APICommunications()
        //APISource.fetchFromFile { (response: FiveDaysForecast?) in
        APISource.fetch { (response: FiveDaysForecast?) in
            self.response = response
            DispatchQueue.main.async(execute: {
                self.temperature.text = String(format:"%.1f", (response?.days[0].main.temp)!)
                self.desc.text = response?.days[0].weather.description
                self.icon.image = UIImage(systemName: self.mapIcon(weather: response?.days[0].weather.main))
                self.wind.text = String(format:"%.1f", (response?.days[0].wind.speed)!) + " kmh"
                self.tableView?.reloadData()
            })
        }
    }
    
    private func mapIcon(weather: String?) -> String {
        switch weather {
        case "Clouds":
            return "cloud"
        case "Clear":
            return "sun.max"
        case "Rain":
            return "cloud.rain"
        case "Thunderstorm":
            return "cloud.bolt"
        case "Drizzle":
            return "cloud.drizzle"
        case "Snow":
            return "snow"
        default:
            return "cloud.sun.rain"
        }
    }
    
    // UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = response?.days.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainViewCell", for: indexPath) as! MainViewCell
        let date = response?.days[indexPath.row].date
        let timezone = TimeZone.current.abbreviation() ?? "CET"
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: timezone)
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "E dd"
        cell.dateLabel.text = dateFormatter.string(from: date ?? Date())
        cell.minTempLabel.text = String(format:"%.1f", (response?.days[indexPath.row].main.temp_min)!)
        cell.maxTempLabel.text = String(format:"%.1f", (response?.days[indexPath.row].main.temp_max)!)
        cell.descriptionLabel.text = response?.days[indexPath.row].weather.main
        cell.iconImage.image = UIImage(systemName: mapIcon(weather: response?.days[indexPath.row].weather.main))
        return cell
    }

    // UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow!
        performSegue(withIdentifier: "detailSegue", sender: response?.days[indexPath.row])
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detailSegue") {
            let detailController = segue.destination as! DetailViewController
            detailController.detailForecast = sender as? DayForecast
        }
    }
}

class MainViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
}
