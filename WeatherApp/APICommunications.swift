//APICommunications.swift created on 13/02/2020

import Foundation

// API Structure
struct OpenWeatherAPI: Decodable, Hashable {
    let cod: String
    let message: Int
    let cnt: Int
    let list: [OpenWeatherAPIHourly]
    let city: OpenWeatherAPICity
}

struct OpenWeatherAPIHourly: Decodable, Hashable {
    let dt: Double
    let main: OpenWeatherAPIHourlyMain
    let weather: [OpenWeatherAPIWeatherMain]
    let clouds: OpenWeatherAPICloudsMain
    let wind: OpenWeatherAPIWindMain
}

struct OpenWeatherAPIHourlyMain: Decodable, Hashable {
    let temp: Double
    let temp_min: Double
    let temp_max: Double
}

struct OpenWeatherAPIWeatherMain: Decodable, Hashable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct OpenWeatherAPICloudsMain: Decodable, Hashable {
    let all: Int
}

struct OpenWeatherAPIWindMain: Decodable, Hashable {
    let speed: Double
    let deg: Int
}

struct OpenWeatherAPICity: Decodable, Hashable {
    let id: Int
    let name: String
    let coord: CityCoordinates
    let country: String
    let timezone: Int
    let sunrise: Int
    let sunset: Int
}

struct CityCoordinates: Decodable, Hashable {
    let lat: Double
    let lon: Double
}

struct FiveDaysForecast {
    var days: [DayForecast]
}

struct DayForecast {
    var date: Date
    var main: OpenWeatherAPIHourlyMain
    var weather: OpenWeatherAPIWeatherMain
    let wind: OpenWeatherAPIWindMain
    var hours: HourForecast
}

struct HourForecast {
    var list: [OpenWeatherAPIHourly]
}

class APICommunications {
    let APIURL = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=Milano,it&appid=8781948dc59ca96a07e7a863ca8be211&units=metric")

    func fetch(completionHandler: @escaping (FiveDaysForecast?) -> Void) {
        let request = URLRequest(url: APIURL!)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                fatalError("Error: \(error.localizedDescription)")
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                fatalError("Error: invalid HTTP response code")
            }
            guard let data = data else {
                fatalError("Error: missing response data")
            }

            do {
                let decoder = JSONDecoder()
                let weatherData = try decoder.decode(OpenWeatherAPI.self, from: data)
                completionHandler(self.processData(weatherData: weatherData))
            }
            catch {
                print("Error: \(error.localizedDescription)")
            }
            
        }
        task.resume()
    }

    func fetchFromFile(completionHandler: @escaping (FiveDaysForecast?) -> Void) {
        do {
            let decoder = JSONDecoder()
            let filePath = Bundle.main.url(forResource: "forecast", withExtension: "json")
            let data = try Data(contentsOf: filePath!)
            let weatherData = try decoder.decode(OpenWeatherAPI.self, from: data)
            completionHandler(self.processData(weatherData: weatherData))
        }
        catch {
            print("Error: \(error.localizedDescription)")
        }

    }
    
    func processData(weatherData: OpenWeatherAPI) -> FiveDaysForecast {
        var fiveDaysForecast: FiveDaysForecast = FiveDaysForecast(days: [])
        let calendar = Calendar.current
        var index: Int = 0
        // cycle through days
        while index < weatherData.list.count {
            let date = Date(timeIntervalSince1970: weatherData.list[index].dt)
            var nextIndex = index
            var minTemp = 100.0
            var maxTemp = 0.0
            var main = "Clear"
            var hourly: [OpenWeatherAPIHourly] = []
            // find next day and collect weather data for current day
            while nextIndex < weatherData.list.count {
                hourly.append(weatherData.list[nextIndex])
                minTemp = min(minTemp, weatherData.list[nextIndex].main.temp_min)
                maxTemp = max(maxTemp, weatherData.list[nextIndex].main.temp_max)
                if weatherData.list[nextIndex].weather[0].main != "Clear" {
                    main = weatherData.list[nextIndex].weather[0].main
                }
                let nextDate = Date(timeIntervalSince1970: weatherData.list[nextIndex].dt)
                if calendar.compare(date, to: nextDate, toGranularity: .day) == .orderedSame {
                    nextIndex += 1
                    continue
                }
                break
            }
            let hourlyMain = OpenWeatherAPIHourlyMain(temp: weatherData.list[index].main.temp, temp_min: minTemp, temp_max: maxTemp)
            let weatherMain = OpenWeatherAPIWeatherMain(id: weatherData.list[index].weather[0].id, main: main, description: weatherData.list[index].weather[0].description, icon: weatherData.list[index].weather[0].icon)
            fiveDaysForecast.days.append(DayForecast(date: date, main: hourlyMain, weather: weatherMain, wind: weatherData.list[index].wind, hours: HourForecast(list: hourly)))
            index = nextIndex
        }
        return fiveDaysForecast
    }

}
