//DetailViewController.swift created on 14/02/2020

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    var detailForecast: DayForecast?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let date = Date(timeIntervalSince1970: (detailForecast?.hours.list[0].dt)!)
        let timezone = TimeZone.current.abbreviation() ?? "CET"
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MMM dd, E"
        dateFormatter.string(from: date ?? Date())
        dateLabel.text = dateFormatter.string(from: date ?? Date())
        let minTemp = String(format:"%.1f", (detailForecast?.hours.list.reduce(100) { (x: Double, y: OpenWeatherAPIHourly) -> Double in
            return min(x, y.main.temp_min)
            })!)
        dateFormatter.string(from: date ?? Date())
        let maxTemp = String(format:"%.1f", (detailForecast?.hours.list.reduce(-100) { (x: Double, y: OpenWeatherAPIHourly) -> Double in
            return max(x, y.main.temp_min)
            })!)
        tempLabel.text = minTemp + " / " + maxTemp
    }
    
    // UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = detailForecast?.hours.list.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailViewCell", for: indexPath) as! DetailViewCell
        let date = Date(timeIntervalSince1970: (detailForecast?.hours.list[indexPath.row].dt)!)
        let timezone = TimeZone.current.abbreviation() ?? "CET"
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH"
        cell.hourLabel.text = dateFormatter.string(from: date ?? Date())
        cell.tempLabel.text = String(format:"%.1f", (detailForecast?.hours.list[indexPath.row].main.temp)!)
        cell.descLabel.text = detailForecast?.hours.list[indexPath.row].weather[0].description
        return cell
    }

    // UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
}

class DetailViewCell: UITableViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
}

